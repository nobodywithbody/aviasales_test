import React, {
  FC,
  useState,
  useContext,
  useCallback,
  memo,
  useMemo
} from "react";
import { Title } from "@components/Title";
import { Checkbox } from "@components/Checkbox";
import { Container } from "@components/Container";
import { Context } from "@contexts/Form";
import { IRenderTransfers, Inputs } from "@types";

const renderTransfers = (transfers: IRenderTransfers) => {
  return transfers.map(({ key, handleChange, value, label }) => (
    <Checkbox key={key} onChange={handleChange} checked={value}>
      {label}
    </Checkbox>
  ));
};

const TransferBlock: FC = memo(() => {
  const [checked, setChecked] = useState(false);
  const { transfers, getUpdater } = useContext(Context);
  const transfersUpdater = getUpdater("transfers");
  const transfersWithCallback = useMemo(
    () =>
      transfers.map(({ key, ...rest }) => ({
        key,
        ...rest,
        handleChange: transfersUpdater(key as Inputs)
      })),
    [transfers]
  );

  const handleAllTransfers = useCallback(
    value => {
      setChecked(value);
      transfersWithCallback.forEach(transfer => transfer.handleChange(value));
    },
    [transfersWithCallback]
  );

  return (
    <>
      <Container>
        <Title>количество пересадок</Title>
      </Container>
      <Checkbox checked={checked} onChange={handleAllTransfers}>
        Все
      </Checkbox>
      {renderTransfers(transfersWithCallback)}
    </>
  );
});

export { TransferBlock };
