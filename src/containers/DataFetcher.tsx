import React, { FC, useContext, memo } from "react";
import { useSearchFetcher } from "@utils";
import { Block } from "@components/Block";
import { List } from "react-content-loader";
import { Container } from "@components/Container";
import styled from "styled-components";
import { IFormState, FormCategory, TicketWithId } from "@types";
import { TransferCard } from "@components/TransferCard";
import { Box } from "@components/Box";
import { Context } from "@contexts/Form";
import { useTransition, animated } from "react-spring";

const ErrorContainer = styled(Container)`
  background: #ffeaea;
  font-weight: 400;
  color: #3e3e3e;
  border-radius: 5px;
  margin-bottom: 20px;
`;

const filterCards = (
  cards: TicketWithId[],
  { transfers, most }: Pick<IFormState, FormCategory>
) => {
  const isChip = most.find(el => el.key === "chip").value;
  const availableStops = transfers.filter(el => el.value).map(el => el.key);

  return cards
    .filter(
      card =>
        availableStops.includes(card.segments[0].stops.length) &&
        availableStops.includes(card.segments[1].stops.length)
    )
    .sort((a, b) => {
      if (isChip) {
        return a.price - b.price;
      }

      return (
        a.segments[0].duration +
        a.segments[1].duration -
        (b.segments[0].duration + b.segments[1].duration)
      );
    })
    .slice(0, 5);
};

const DataFetcher: FC = memo(() => {
  const { transfers, most } = useContext(Context);
  const { isLoading, isError, data } = useSearchFetcher();
  const filters = {
    transfers,
    most
  };
  const visibleCards = filterCards(data, filters);

  const transitions = useTransition(visibleCards, el => el.id, {
    from: { transform: "translateX(300px)", opacity: 0, maxHeight: 0 },
    enter: { transform: "translateX(0)", opacity: 1, maxHeight: 300 },
    leave: { transform: "translateX(300px)", opacity: 0, maxHeight: 0 }
  });

  if (isLoading) {
    return (
      <Block>
        <Container>
          <List />
        </Container>
      </Block>
    );
  }

  return (
    <>
      {isError && data.length > 0 && (
        <ErrorContainer>
          К сожалению, не удалось загрузить данные полностью. Мы выбрали
          подходящее из <strong>{data.length}</strong>. Пока вы смотрите из
          существующих, мы думаем как доставить вам остальные варианты🤔
        </ErrorContainer>
      )}
      {isError && data.length === 0 && (
        <ErrorContainer>
          К сожалению, не удалось загрузить данные. Но вы можете позвонить на по
          телефону и мы что-нибудь вам подберем.
        </ErrorContainer>
      )}
      {!isLoading &&
        transitions.length > 0 &&
        transitions.map(({ item: card, props, key }) => {
          return (
            <animated.div key={key} style={props}>
              <Box mb={20}>
                <TransferCard
                  carrier={card.carrier}
                  price={card.price}
                  toOrigin={card.segments[0].origin}
                  toDestination={card.segments[0].destination}
                  toStops={card.segments[0].stops}
                  toDate={card.segments[0].date}
                  toDuration={card.segments[0].duration}
                  backOrigin={card.segments[1].origin}
                  backDestination={card.segments[1].destination}
                  backStops={card.segments[1].stops}
                  backDate={card.segments[1].date}
                  backDuration={card.segments[1].duration}
                />
              </Box>
            </animated.div>
          );
        })}
      {!isLoading && data.length > 0 && transitions.length === 0 && (
        <Block>
          <Container>
            К сожалению по заданному фильтру не удалось ничего найти, попробуйте
            менее строгие критерии.
          </Container>
        </Block>
      )}
    </>
  );
});

export { DataFetcher };
