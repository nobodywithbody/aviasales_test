import React, { useContext, useCallback, memo } from "react";
import { Context } from "@contexts/Form";
import { ButtonRadio } from "@components/ButtonRadio";
import { Inputs } from "@types";

const MostBlock = memo(() => {
  const { getUpdater, most } = useContext(Context);

  const mostUpdater = getUpdater("most");
  const active = most.find(el => el.value).key;
  const handleChange = useCallback(
    (curOption: string) => {
      most.forEach(({ key }) => {
        if (curOption === key) {
          return mostUpdater(key as Inputs)(true);
        }

        mostUpdater(key as Inputs)(false);
      });
    },
    [mostUpdater, most]
  );

  return <ButtonRadio active={active} onChange={handleChange} options={most} />;
});

export { MostBlock };
