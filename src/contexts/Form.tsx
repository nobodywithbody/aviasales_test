import React, { createContext, FC, useReducer, useCallback } from "react";
import { setIn } from "immutable";
import { IFormState, FormAction, Inputs, FormCategory } from "@types";

const UPDATE_VALUE = "UPDATE_VALUE";

const defaultState = {
  transfers: [
    {
      key: 0,
      label: "Без пересадок",
      value: true
    },
    {
      key: 1,
      label: "1 пересадка",
      value: true
    },
    {
      key: 2,
      label: "2 пересадки",
      value: false
    },
    {
      key: 3,
      label: "3 пересадки",
      value: false
    }
  ],
  most: [
    {
      key: "chip",
      label: "Самый дешевый",
      value: true
    },
    {
      key: "fast",
      label: "Самый быстрый",
      value: false
    }
  ]
};

const reducer = (
  state: Pick<IFormState, FormCategory>,
  { payload: { category, input, value }, type }: FormAction
) => {
  switch (type) {
    case UPDATE_VALUE:
      const index = state[category].findIndex(el => el.key === input);
      return setIn(state, [category, index, "value"], value);
    default:
      return state;
  }
};

const Context = createContext<IFormState>({
  ...defaultState,
  getUpdater: (category: FormCategory) => (input: Inputs) => (
    value: boolean
  ) => {}
});

const Form: FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, defaultState);

  const getUpdater = useCallback(
    (category: FormCategory) => (input: Inputs) => (value: boolean) =>
      dispatch({
        type: UPDATE_VALUE,
        payload: {
          category,
          input,
          value
        }
      }),
    [dispatch]
  );

  const contextState = {
    ...state,
    getUpdater
  };

  return <Context.Provider value={contextState}>{children}</Context.Provider>;
};

export { Form, Context };
