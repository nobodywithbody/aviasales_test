import React, { FC, useCallback, memo } from "react";
import styled from "styled-components";
import { ICheckboxProps } from "@types";
import CheckboxMark from "@assets/icons/CheckboxMark.svg";
import { Flex } from "./Flex";

const Mark = styled.div`
  border: 1px solid #2196f3;
  border-radius: 2px;
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 10px;
`;

const Icon = styled(CheckboxMark)`
  width: 12px;
  height: 8px;
`;

const Label = styled.label`
  font-size: 13px;
  color: #4a4a4a;
  padding: 10px 20px;
  user-select: none;

  &:hover {
    background: #f1fcff;
  }
`;

const Input = styled.input`
  width: 0;
  height: 0;
  display: none;
`;

const Checkbox: FC<ICheckboxProps> = memo(({ children, checked, onChange }) => {
  const handleChange = useCallback(() => {
    onChange(!checked);
  }, [checked]);

  return (
    <Flex as={Label} alignItems="center" px="20">
      <Mark>{checked && <Icon />}</Mark>

      <Input checked={checked} type="checkbox" onChange={handleChange}></Input>
      {children}
    </Flex>
  );
});

export { Checkbox };
