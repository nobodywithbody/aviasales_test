import React, { FC, memo } from "react";
import { IButtonRadioProps } from "@types";
import styled from "styled-components";
import { Flex } from "./Flex";
import { IButtonProps } from "@types";

const selectBorderRadius = (enable: boolean) => (enable ? "5px" : 0);

const Button = styled.button<IButtonProps>`
  border: 1px solid #dfe5ec;
  margin: 0;
  padding: 0;
  width: auto;
  overflow: visible;
  color: inherit;
  font: inherit;
  line-height: normal;
  -webkit-font-smoothing: inherit;
  -moz-osx-font-smoothing: inherit;
  -webkit-appearance: none;
  width: 100%;
  letter-spacing: 0.5px;
  text-transform: uppercase;
  font-weight: 600;
  font-size: 12px;
  line-height: 20px;
  padding-top: 15px;
  padding-bottom: 15px;
  flex-wrap: wrap;
  outline: none;
  &:not(first-of-type) {
    margin-left: -1px;
  }

  &:hover,
  &:active {
    outline: none;
  }

  ${({ tail, head, active }) => ({
    borderTopLeftRadius: selectBorderRadius(head),
    borderBottomLeftRadius: selectBorderRadius(head),
    borderTopRightRadius: selectBorderRadius(tail),
    borderBottomRightRadius: selectBorderRadius(tail),
    background: active ? "#2196F3" : "#ffffff",
    color: active ? "#FFFFFF" : "#4A4A4A"
  })}
`;

const ButtonRadio: FC<IButtonRadioProps> = memo(
  ({ options, active, onChange }) => {
    return (
      <Flex>
        {options.map(({ label, key }, index) => {
          return (
            <Button
              head={index === 0}
              tail={index === options.length - 1}
              key={key}
              active={active === key}
              onClick={() => onChange(key)}
            >
              {label}
            </Button>
          );
        })}
      </Flex>
    );
  }
);

export { ButtonRadio };
