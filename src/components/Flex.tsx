import styled from "styled-components";
import {
  width,
  height,
  maxWidth,
  fontSize,
  space,
  backgroundImage,
  backgroundRepeat,
  backgroundSize,
  lineHeight,
  flex,
  justifyContent,
  alignItems,
  flexDirection,
  alignContent
} from "styled-system";
import { FlexType } from "@types";
import React, { PropsWithChildren, ReactElement } from "react";

const FlexWrapper = styled.div<FlexType>(
  {
    display: "flex"
  },
  width,
  height,
  maxWidth,
  fontSize,
  space,
  backgroundImage,
  backgroundRepeat,
  backgroundSize,
  lineHeight,
  flex,
  justifyContent,
  alignItems,
  flexDirection,
  alignContent
);

FlexWrapper.displayName = "Styled(Flex)";

const Flex = <T extends {}>(
  params: PropsWithChildren<FlexType & T>
): ReactElement | null => {
  return <FlexWrapper {...params} />;
};

Flex.displayName = "Flex";

export { Flex };
