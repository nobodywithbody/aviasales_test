import React, { FC, memo } from "react";
import { IVerticalValue } from "@types";
import styled from "styled-components";
import { Flex } from "./Flex";

const Label = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 18px;
  letter-spacing: 0.5px;
  text-transform: uppercase;

  color: #a0b0b9;
`;

const Value = styled.div`
  font-size: 14px;
  line-height: 21px;
  height: 21px;
`;

const VerticalValue: FC<IVerticalValue> = memo(({ label, children }) => {
  return (
    <Flex flexDirection="column">
      <Label>{label}</Label>
      <Value> {children} </Value>
    </Flex>
  );
});

export { VerticalValue };
