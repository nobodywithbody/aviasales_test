import React, { memo, FC } from "react";
import { VerticalValue } from "@components/VerticalValue";
import moment from "moment";
import { ITimeValueProps } from "@types";

const TimeValue: FC<ITimeValueProps> = memo(({ label, from, duration }) => {
  const fromTime = moment(from);
  const endTime = fromTime.clone().add(duration, "m");
  return (
    <VerticalValue label={label}>
      {fromTime.format("HH:mm")} – {endTime.format("HH:mm")}
    </VerticalValue>
  );
});

export { TimeValue };
