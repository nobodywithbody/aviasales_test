import React, { FC, memo } from "react";
import { IPriceProps } from "@types";
import styled from "styled-components";

const PriceStyled = styled.div`
  color: #2196f3;
  font-weight: 600;
  font-size: 24px;
  line-height: 24px;
`;

const Price: FC<IPriceProps> = memo(({ value }) => {
  const formattedPrice = new Intl.NumberFormat("ru-RU").format(value);

  return <PriceStyled> {formattedPrice} ₽ </PriceStyled>;
});

export { Price };
