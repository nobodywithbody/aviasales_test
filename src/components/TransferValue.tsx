import React, { FC, memo } from "react";
import { plural } from "@utils";
import { VerticalValue } from "./VerticalValue";
import { ITransferValueProps } from "@types";

const TransferValue: FC<ITransferValueProps> = memo(({ transfers }) => {
  const value = plural(
    ["пересадка", "пересадки", "пересадок"],
    transfers.length
  );

  return (
    <VerticalValue label={`${transfers.length} ${value}`}>
      {transfers.join(", ")}
    </VerticalValue>
  );
});

export { TransferValue };
