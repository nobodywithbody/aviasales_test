import React, { memo, FC } from "react";
import { VerticalValue } from "@components/VerticalValue";
import { IDurationValueProps } from "@types";

const DurationValue: FC<IDurationValueProps> = memo(({ minutes }) => {
  const days = Math.floor(minutes / 60 / 24);
  const hours = Math.floor(minutes / 60) - days * 24;
  const minutesLeft = minutes - days * 60 * 24 - hours * 60;

  const times = {
    д: days,
    ч: hours,
    м: minutesLeft
  };

  const result = Object.entries(times).reduce((acc, [label, value]) => {
    if (!value) return acc;

    return `${acc} ${value}${label}`;
  }, "");

  return <VerticalValue label="В пути">{result}</VerticalValue>;
});

export { DurationValue };
