import React, { FC, memo } from "react";
import { Block } from "./Block";
import { Flex } from "./Flex";
import { Container } from "./Container";
import { Price } from "./Price";
import { AviaCompany } from "./AviaCompany";
import { Box } from "./Box";
import { TimeValue } from "./TimeValue";
import { DurationValue } from "./DurationValue";
import { TransferValue } from "./TransferValue";
import { ITrasferCardProps } from "@types";

const TransferCard: FC<ITrasferCardProps> = memo(
  ({
    carrier,
    price,
    toOrigin,
    toDestination,
    toDate,
    toStops,
    toDuration,
    backOrigin,
    backDestination,
    backDate,
    backStops,
    backDuration
  }) => {
    return (
      <Block>
        <Container>
          <Flex flexDirection="column">
            <Flex flexDirection="row" justifyContent="space-between" mb={20}>
              <Price value={price} />
              <AviaCompany code={carrier} />
            </Flex>
            <Flex justifyContent="space-between">
              <Box>
                <Box mb={10}>
                  <TimeValue
                    label={`${toOrigin} - ${toDestination}`}
                    from={toDate}
                    duration={toDuration}
                  />
                </Box>
                <TimeValue
                  label={`${backOrigin} - ${backDestination}`}
                  from={backDate}
                  duration={backDuration}
                />
              </Box>
              <Box>
                <Box mb={10}>
                  <DurationValue minutes={toDuration} />
                </Box>
                <DurationValue minutes={backDuration} />
              </Box>
              <Box>
                <Box mb={10}>
                  <TransferValue transfers={toStops} />
                </Box>
                <TransferValue transfers={backStops} />
              </Box>
            </Flex>
          </Flex>
        </Container>
      </Block>
    );
  }
);

export { TransferCard };
