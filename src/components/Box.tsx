import styled from 'styled-components';
import {
  width,
  height,
  maxWidth,
  fontSize,
  space,
  backgroundImage,
  backgroundRepeat,
  backgroundSize,
  lineHeight,
  flex,
} from 'styled-system'
import { BoxType } from '@types'
import React, { PropsWithChildren, ReactElement } from 'react'

const BoxWrapper = styled.div<BoxType>(
  {
    boxSizing: 'border-box',
    minWidth: 0,
  },
  width,
  height,
  maxWidth,
  fontSize,
  space,
  backgroundImage,
  backgroundRepeat,
  backgroundSize,
  lineHeight,
  flex,
)

BoxWrapper.displayName = 'Styled(Box)'

const Box = <T extends {}>(
  params: PropsWithChildren<BoxType & T>,
): ReactElement | null => {
  return <BoxWrapper {...params} />
}

Box.displayName = 'Box'

export { Box }
