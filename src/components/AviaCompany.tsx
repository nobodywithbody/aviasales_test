import React, { FC, memo } from "react";
import { IAviaCompany } from "@types";

const AviaCompany: FC<IAviaCompany> = memo(({ code }) => {
  return <img src={`//pics.avs.io/99/36/${code}.png`}></img>;
});

export { AviaCompany };
