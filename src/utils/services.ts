import Axios from "axios";
import { ISearchIdResponse, ISearchResponse } from "@types";

export const getSearchId = async () =>
  await Axios.get<ISearchIdResponse>(
    "https://front-test.beta.aviasales.ru/search"
  );

export const getSearch = async (searchId: number) =>
  await Axios.get<ISearchResponse>(
    `https://front-test.beta.aviasales.ru/tickets?searchId=${searchId}`
  );
