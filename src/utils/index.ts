import { useEffect, useState } from "react";
import { ITicket, TicketWithId } from "@types";
import { getSearchId, getSearch } from "./services";

export const plural = (forms: string[], number: number): string => {
  let n = Math.abs(number);
  n %= 100;
  if (n >= 5 && n <= 20) {
    return forms[2];
  }
  n %= 10;
  if (n === 1) {
    return forms[0];
  }
  if (n >= 2 && n <= 4) {
    return forms[1];
  }
  return forms[2];
};

export const useSearchFetcher = () => {
  const [data, setData] = useState<TicketWithId[]>([]);
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetch = async () => {
      try {
        let id = 0;
        const {
          data: { searchId }
        } = await getSearchId();

        if (!searchId) {
          throw new Error("missed searchId");
        }
        let dataLoaded = false;
        let data: ITicket[] = [];
        while (!dataLoaded) {
          const {
            data: { tickets, stop }
          } = await getSearch(searchId);

          data = [...data, ...tickets];

          if (stop) {
            dataLoaded = true;
          }
          setData(
            data.map(el => ({
              ...el,
              id: ++id
            }))
          );
        }
      } catch (e) {
        console.error(e);
        setIsError(true);
      } finally {
        setIsLoading(false);
      }
    };
    console.log("fetch");
    fetch();
  }, []);

  return { isLoading, isError, data };
};
