import React from "react";
import { createGlobalStyle } from "styled-components";
import { Block } from "@components/Block";
import { Flex } from "@components/Flex";
import { Box } from "@components/Box";
import Logo from "@assets/icons/Logo.svg";
import { TransferBlock } from "@containers/TransferBlock";
import { MostBlock } from "@containers/MostBlock";
import { Form } from "@contexts/Form";
import { DataFetcher } from "@containers/DataFetcher";

const GlobalStyle = createGlobalStyle`
  body, html {
    font-family: 'Open Sans', sans-serif;
    padding: 0;
    margin: 0;
    background: #f3f7fa;
  }
`;

const App = () => {
  return (
    <Form>
      <GlobalStyle />
      <Flex px={[30, 30, 0]} alignItems="center" flexDirection="column">
        <Box as={Logo} my="50" />
        <Flex maxWidth="754" width="100%" flexDirection={["column", "row"]}>
          <Box mr="20" width={[1, 1, 4 / 12]} mb={[20, 0]}>
            <Box as={Block} pb="10">
              <TransferBlock />
            </Box>
          </Box>
          <Box width={[1, 1, 8 / 10]}>
            <Box mb={20}>
              <MostBlock />
            </Box>
            <DataFetcher />
          </Box>
        </Flex>
      </Flex>
    </Form>
  );
};

export { App };
