declare module '*.svg' {
  const content: any
  export default content
}

declare module '*.png' {
  const value: any
  export = value
}

declare module '*.jpg' {
  const value: any
  export = value
}

declare module 'react-click-outside' {
  import { FC } from 'react'

  const ClickOutside: FC<{ onClickOutside: () => void }>

  export = ClickOutside
}

declare module 'console' {
  export function log(message: any): void
  export function error(message: any): void
}
