import {
  MaxWidthProps,
  WidthProps,
  JustifyContentProps,
  FontSizeProps,
  SpaceProps,
  AlignItemsProps,
  FlexDirectionProps,
  AlignContentProps,
  BackgroundImageProps,
  BackgroundSizeProps,
  BackgroundRepeatProps,
  HeightProps,
  LineHeightProps,
  FlexProps
} from "styled-system";

export type Pick<T, K extends keyof T> = { [P in K]: T[P] };

export type BoxType = MaxWidthProps &
  WidthProps &
  FontSizeProps &
  SpaceProps &
  BackgroundImageProps &
  BackgroundSizeProps &
  BackgroundRepeatProps &
  HeightProps &
  LineHeightProps &
  FlexProps;

export type FlexType = JustifyContentProps &
  AlignItemsProps &
  FlexDirectionProps &
  AlignContentProps &
  BoxType;

export interface ICheckboxProps {
  checked: boolean;
  onChange(attr: boolean): void;
}

export type FormInput = {
  key: string | number;
  value: boolean;
  label: string;
};

export type UpdaterType = (
  input: 0 | 1 | 2 | 3 | "chip" | "most"
) => (value: boolean) => void;

export type GetUpdaterType = (category: FormCategory) => UpdaterType;

export interface IFormState {
  transfers: FormInput[];
  most: FormInput[];
  getUpdater: GetUpdaterType;
}

export type IRenderTransfers = Array<
  {
    handleChange(attr: boolean): void;
  } & FormInput
>;

export type FormCategory = "transfers" | "most";

export type FormAction = {
  type: "UPDATE_VALUE";
  payload: {
    category: FormCategory;
    input: Inputs;
    value: boolean;
  };
};

export type Inputs = 0 | 1 | 2 | 3 | "chip" | "most";

export interface IButtonRadioProps {
  options: {
    key: string | number;
    label: string;
  }[];
  onChange(buttonKey: string | number): void;
  active: string | number;
}

export interface IButtonProps {
  tail: boolean;
  head: boolean;
  active: boolean;
}

export interface IPriceProps {
  value: number;
}

export interface IAviaCompany {
  code: string;
}

export interface IVerticalValue {
  label: string;
}

export interface ITimeValueProps {
  label: string;
  from: string;
  duration: number;
}

export interface IDurationValueProps {
  minutes: number;
}

export interface ITransferValueProps {
  transfers: string[];
}

export interface ITrasferCardProps {
  carrier: string;
  price: number;
  toOrigin: string;
  toDestination: string;
  toDate: string;
  toStops: string[];
  toDuration: number;
  backOrigin: string;
  backDestination: string;
  backDate: string;
  backStops: string[];
  backDuration: number;
}

export interface ISearchIdResponse {
  searchId: number;
}

export interface ITicket {
  price: number;
  carrier: string;
  segments: [
    {
      origin: string;
      destination: string;
      date: string;
      stops: string[];
      duration: number;
    },
    {
      origin: string;
      destination: string;
      date: string;
      stops: string[];
      duration: number;
    }
  ];
}

export type TicketWithId = ITicket & { id: number };
export interface ISearchResponse {
  tickets: ITicket[];
  stop: boolean;
}
