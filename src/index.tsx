import ReactDOM from "react-dom";
import React from "react";
import { App } from "./pages/App";
import WebFont from "webfontloader";
import { hot } from "react-hot-loader/root";

WebFont.load({
  google: {
    families: [
      "Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i",
      "sans-serif"
    ]
  }
});

const HocedApp = hot(App);

ReactDOM.render(<HocedApp />, document.getElementById("app"));
