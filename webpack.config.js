const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: "./src/index.tsx",
  output: {
    path: __dirname + "/public",
    publicPath: process.env.PUBLIC_PATH || "/",
    filename: "[hash].bundle.js"
  },
  stats: "errors-only",
  resolve: {
    extensions: [".tsx", ".ts", ".json", ".js"],
    alias: {
      "react-dom": "@hot-loader/react-dom",
      "@components": path.resolve(__dirname, "src/components"),
      "@containers": path.resolve(__dirname, "src/containers"),
      "@types": path.resolve(__dirname, "src/types"),
      "@utils": path.resolve(__dirname, "src/utils"),
      "@assets": path.resolve(__dirname, "src/assets"),
      "@contexts": path.resolve(__dirname, "src/contexts")
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "src/index.ejs",
      title: "aviasales test"
    }),
    new webpack.EnvironmentPlugin({ PUBLIC_PATH: "" })
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.(tsx?)$/,
        exclude: /node_modules/,
        loader: "ts-loader"
      },
      {
        test: /\.svg$/,
        use: ["@svgr/webpack"]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: ["file-loader"]
      }
    ]
  },
  devServer: {
    historyApiFallback: true,
    watchOptions: {
      ignored: /node_modules/
    }
  }
};
