# How to use

**To order start dev-server you need**

- yarn - install dependecies
- yarn start - start dev server

**To order start production-ready build**

- yarn - install dependecies
- yarn build - build static bundle
- http-server public - you need something for serving data. I use [http-server](https://www.npmjs.com/package/http-server)

# Tested in 

* chrome
* firefox